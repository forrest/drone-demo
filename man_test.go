package main

import (
	"gopkg.in/mgo.v2"
	"testing"
)

func Test_connmgo(t *testing.T) {
	session, err := mgo.Dial("database:27017")
	if err != nil {
		panic(err)
	}
	defer session.Close()
}
